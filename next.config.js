const withTypescript = require('@zeit/next-typescript')
const withSass = require('@zeit/next-sass')
const withBundleAnalyzer = require('@zeit/next-bundle-analyzer')

module.exports = withTypescript(withSass(withBundleAnalyzer({
  ...analyzerOptions(),
  publicRuntimeConfig: { // Will be available on both server and client
    config_loaded: true,
    some_env_var: 12345
  },
  sassLoaderOptions: {
    includePaths: ['pages', 'pages/styles']
  },
})))

function analyzerOptions() {

  return {
    analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
    analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
    bundleAnalyzerConfig: {
      server: {
        analyzerMode: 'static',
        reportFilename: '../../bundles/server.html'
      },
      browser: {
        analyzerMode: 'static',
        reportFilename: '../bundles/client.html'
      }
    }
  }
}
