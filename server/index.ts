import next from 'next'
import { ApiServer as Server } from 'server/server'
import { parse } from 'url'
import { data } from 'lib/data.service'

import { env } from 'config'

console.log('Server can use services too! =>', data.sort().join(''))
const server = new Server()
const app = next(env.next)
const handle = app.getRequestHandler()

const renderer = (req, res) => handle(req, res, parse(req.url, true));

const startServer = () =>
    server
        .use(renderer)
        .start(env.PORT)




app.prepare()
    .then(startServer)
