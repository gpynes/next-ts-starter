import { Application, RequestHandler } from 'express'
import bodyParser from 'body-parser'
import { Server } from '@overnightjs/core'
import { UserController } from './controller'

export class ApiServer extends Server {
    public expressApp: Application
    constructor() {
        super()
        
        this.setupExpress()
        const ctrlsArr = this.setupControllers()
        
        super.addControllers(ctrlsArr)
    }

    private setupExpress(): void {
        
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({extended: true}))
    }

    private setupControllers(): Array<UserController> {
        
        let userController = new UserController()
        

        return [userController]
    }

    public start(port: number): void {
        
        this.app.listen(port, () =>
            console.log('Server listening on port:' + port))
    }

    public use(handler: RequestHandler): ApiServer {
        this.app.use(handler)
        return this
    }
}
