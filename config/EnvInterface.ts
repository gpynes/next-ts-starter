import next from 'next'

export interface ENV {
  PORT: number
  next: next.ServerOptions
}
