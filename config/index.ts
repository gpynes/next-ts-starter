import { ENV } from './EnvInterface'
export * from './EnvInterface'

export enum ENVMODES {
  production = 'production',
  PRODUCTION = 'PRODUCTION',
  dev = 'dev',
  DEV = 'DEV',
  test = 'test',
  TEST = 'TEST',
}

const ENVMODE = ENVMODES[process.env.NODE_ENV] || 'development'

import loadEnv from './environments'
export const env: ENV = loadEnv(ENVMODE)
