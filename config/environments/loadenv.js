const { readFileSync, existsSync } = require('fs')
const envFile = `${process.cwd()}/.env`

const requireEnvFile = (path) => {
  try {
    return require(path)
  } catch (e) {
    console.log('ENVIRONMENT FILE REQUIRED =>', path)
    throw new Error(`e.message\nENVIRONMENT FILE REQUIRED =>${path}`)
  }
}

const ENVS = {
  development: requireEnvFile('./development'),
  production: requireEnvFile('./production')
}

module.exports = function (envType) {
  console.log(`loading => "${envType}" mode.`)
  const jsFileEnv = ENVS[envType] || ENVS.development
  return {
    ...jsFileEnv,
    ...loadEnvFile(envFile)
  }
}


function loadEnvFile(path) {
  if (existsSync(path)) {
    const text = readFileSync(path, 'utf8')
    return parseToObject(text)
  }
  return {}
}

function parseToObject(str) {
  return str
    .split('\n')
    .filter(a => !!a) // filter empty lines
    .filter(a => !a.startsWith('#')) // filter comments
    .filter(a => a.includes('=')) // filter non values
    .map(str => str.split('='))
    .reduce((obj, [key, value]) => ({ ...obj, [key]: getValue(value) }), {})
}

// Returns the proper JS typed value of the string. ie: "3000" => 3000
function getValue(value) {
  const typeIs = typeof value
  // Check if number, if so return a number
  if (typeIs === 'number' || !isNaN(+value)) {
    return +value
  }

  // Value is a string
  return value.replace(/(^")|("$)/g, '').replace(/(^')|('$)/g, '')
}
