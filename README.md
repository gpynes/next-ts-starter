# Getting Started
```
clone project

npm i

npm run dev
npm build 
npm start
```

That's it! Happy coding!

# How this works
This is based on [nextjs](https://nextjs.org) please refer to their docs for how things work.

## Getting started quickly

### Pages
The `pages` directory is where all the client side code lives. This includes `pages` (root pages that can be navigated to), `components` (small re-useable ui elements that are used to compose `pages`), `styles` (all the gobal styling for the application) and other necessary configuration files.

### Creating pages
Simply add a new `<page-name>.tsx` file in the `pages` directory and have the default export be the component you wish to display as that new page!

### Path linking
This project is setup in a way so that relative file paths are unnecessary. ie:
<br>`import '../../some/path/'` instead if the path is marked in the `paths` section of the `tsconfig.json` you can simply:
<br>`import 'lib/utils/some-utility'` this makes it much easier to move files and directories around since typescript handles the path linking for you under the hood!

### Lib
The `lib` is a shared directory for services and utilities that both the `server` and the `pages`(client) can use. The code placed here are meant to be stand alone modules that can be shared across any typescript (or js) project regardless of where the code is run. Think `http`, `fetch` like libraries for accessing data from other services and nice helpers that format or normalize data.

### Server
The `server` directory is for setting up any and all `application api endpoints` these can be data fetchers, queriers etc for populating data into the client with. 

### Config
The `config` directory is where `environment variables` and other `application` specific configuration goes. Please refer to Greg for more information if curious.
