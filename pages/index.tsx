import { data } from 'lib/data.service'
import { Page } from 'components/Page'
import getConfig from 'next/config'
const config = getConfig().publicRuntimeConfig

export default () => (
    <Page>
        <div>
            <p>Client can use services! {data.join('')}</p>
            <p>Client can all get env config! {JSON.stringify(config)}</p>
        </div>
    </Page>
)
