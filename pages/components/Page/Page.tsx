import Head from 'next/head'

import './Page.scss'
export const Page = ({ children }) => (
  <>
    <Head>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
    </Head>
    
    {children}
  </>
)
