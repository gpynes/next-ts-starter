import NextLink from 'next/link'

interface LinkProps {
  href: string,
  children: any
}
export const Link = ({ children, ...props }: LinkProps) => (
  <ActiveLink
    shallow={true}
    prefetch={true}
    {...props}
  >
    {children}
  </ActiveLink>
)


import { withRouter } from 'next/router'
const ActiveLink = withRouter<any>(({ children, router, href, ...props }) => {
  const style = {
    marginRight: 10,
    color: router.pathname === href ? 'green' : 'blue'
  }

  return (
    <NextLink
      href={href}
      {...props}>

      <a href={href} style={style}>
        {children}
      </a>
    </NextLink>
  )
})
